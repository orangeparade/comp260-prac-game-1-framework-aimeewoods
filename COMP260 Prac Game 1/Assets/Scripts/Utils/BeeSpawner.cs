﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{

    public BeeMove beePrefab;
    public PlayerMove player;

    void Start()
    {
        // find a player object to be the target by type
        PlayerMove player =
              (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        target = player.transform;

        // instantiate a bee
        BeeMove bee = Instantiate(beePrefab);
    }
}